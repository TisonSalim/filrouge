package com.afpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.entities.Role;
import com.afpa.dao.IdaoRole;

@RestController

public class RoleController {

	@Autowired
	IdaoRole roledao;
	
	@GetMapping(path = "/roles")
	public List<Role> listeRoles(){
		return this.roledao.findAll();
	}
	
	@PostMapping(path = "/role")
	public void addRole(@RequestBody Role role){
		this.roledao.save(role);
	}
	
	@PutMapping(path = "/role")
	public void putRole(@RequestBody Role role){
//		this.roledao.save(role);
	}
	
	@DeleteMapping(path = "/role/{id}")
	public void delRole(@PathVariable int id){
		this.roledao.deleteById(id);
	}
	
	
}
