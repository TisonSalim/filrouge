package com.afpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.dao.IdaoNiveau;
import com.afpa.entities.Niveau;

@RestController

public class NiveauController {
	@Autowired
	IdaoNiveau niveaudao;
	
	@GetMapping(path = "/niveaux")
	public List<Niveau> listeNiveaux(){
		return this.niveaudao.findAll();
	}
	@PostMapping(path = "/niveau")
	public void addNiveau(@RequestBody Niveau niveau){
		this.niveaudao.save(niveau);
	}
	
	@PutMapping(path = "/niveau")
	public void putNiveau(@RequestBody Niveau niveau){
//		this.niveaudao.save(niveau);
	}
	
	@DeleteMapping(path = "/niveau")
	public void delNiveau(@RequestBody Niveau niveau){
		this.niveaudao.delete(niveau);
	}

}
