package com.afpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.entities.User;
import com.afpa.dao.IdaoUser;

@RestController

public class UserController {
	

	@Autowired
	IdaoUser Userdao;
	
	@GetMapping(path = "/Users")
	public List<User> listeUsers(){
		return this.Userdao.findAll();
	}
	
	@PostMapping(path = "/User")
	public void addUser(@RequestBody User User ){
		this.Userdao.save(User);
	}
	
	@PutMapping(path = "/User")
	public void putRole(@RequestBody User User){
//		this.Userdao.save(User);
	}
	
	@DeleteMapping(path = "/User")
	public void delRole(@RequestBody User User){
		this.Userdao.delete(User);
	}
	

}
