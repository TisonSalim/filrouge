package com.afpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.afpa.entities.Niveau;

import com.afpa.entities.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name="t_user")
public class User {
	@Id
	@GeneratedValue(generator = "USER_SEQ", strategy = GenerationType.SEQUENCE)
	private int id;

	@Column
	private String nom;
	
	@Column
	private String prenom;
	
	@Column
	private String mail;
	
	@Column
	private String password;
	
	@ManyToOne
	private Niveau niveau;
	
	@Column
	private String username;
	
	@ManyToOne
	private Role role;
}
