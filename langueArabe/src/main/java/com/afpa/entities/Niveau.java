
package com.afpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@Builder
@Entity

public class Niveau {
	
	@Id()
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "niveau_seq")
	private int id;
	
	
	@Column()
	private String nom;
	
	@Column
	private String description;
	
	
	
	

}
