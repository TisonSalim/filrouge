package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.entities.Role;

@Repository

public interface IdaoRole extends CrudRepository<Role, Integer> {

	public List<Role> findAll();
}
