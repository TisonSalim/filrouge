package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.entities.User;

@Repository

public interface IdaoUser extends CrudRepository<User, Integer> {
	public List<User> findAll();

	public User findByUsername(String userId);

}
