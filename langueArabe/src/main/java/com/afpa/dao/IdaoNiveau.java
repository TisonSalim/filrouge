package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.entities.Niveau;

@Repository

public interface IdaoNiveau extends CrudRepository<Niveau, Integer>{
	public List<Niveau>findAll();

}
