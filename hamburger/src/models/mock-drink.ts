import Drink from "./drink";


export const DRINKS: Drink[] = [
    {
        id: 1,
        name: "Eau",
        price: 1.50,
        picture: "../assets/boissons/eau.png "
    },
    {
        id: 2,
        name: "Perrier",
        price: 1.50,
        picture: "../assets/boissons/perrier.png "

    },  {
        id: 3,
        name: "Ice Tea Pêche",
        price: 1.50,
        picture: "../assets/boissons/icetea.jpg "

    },  {
        id: 4,
        name: "Ice Tea Framboise",
        price: 1.50,
        picture: "../assets/boissons/iceteaframboise.png "

    },  {
        id: 5,
        name: "Oasis Tropical",
        price: 1.50,
        picture: "../assets/boissons/oasis.png "

    },  {
        id: 6,
        name: "Oasis Pomme Cassis",
        price: 1.50,
        picture: "../assets/boissons/pcf.png "

    },  {
        id: 7,
        name: "Tropico",
        price: 1.50,
        picture: "../assets/boissons/tropico.jpg "

    },  {
        id: 8,
        name: "Coca",
        price: 1.50,
        picture: "../assets/boissons/coca.png "

    },  {
        id: 9,
        name: "Coca Zéro",
        price: 1.50,
        picture: "../assets/boissons/coca0.png "

    },  {
        id: 10,
        name: "Coca Cherry",
        price: 1.50,
        picture: "../assets/boissons/cocacherry.jpeg "

    },
    {
        id: 10,
        name: "Seven Up Mojito",
        price: 1.50,
        picture: "../assets/boissons/7up.png "

    }
];


export default DRINKS;
