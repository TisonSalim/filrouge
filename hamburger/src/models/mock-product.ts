import Product from "./product";

export const PRODUCTS: Product[] = [
    {
        id: 1,
        name: "Fabrik",
        menupicture: "../assets/hamburgers/fabrik.jpg",
        bigpicture: "../assets/hamburgers/moyenfabrik.jpeg",
        littlepicture: "../assets/hamburgers/minifabrik.jpeg",
        epices: "non épicé",
        ingredients: ["Steak,", "Sauce barbecue,", "Cheddar"],
        pricealacard: 8.90,
        priceinmenu: 12.90
    },
    {
        id: 2,
        name: "Classic",
        menupicture: "../assets/hamburgers/classic.jpg",
        bigpicture: "../assets/hamburgers/moyenclassic.jpeg",
        littlepicture: "../assets/hamburgers/miniclassic.jpeg",
        epices: "non épicé",
        ingredients: ["Steak", "Sauce barbecue", "Cheddar"],
        pricealacard: 8.90,
        priceinmenu: 12.90
    },
    {
        id: 3,
        name: "Mexicain",
        menupicture: "../assets/hamburgers/mexicain.jpg",
        bigpicture: "../assets/hamburgers/moyenmexicainfrites.jpeg",
        littlepicture: "../assets/hamburgers/minimexicain.jpeg",
        epices: "non épicé",
        ingredients: ["Steak", "Sauce barbecue", "Cheddar"],
        pricealacard: 8.90,
        priceinmenu: 12.90
    },
    {
        id: 4,
        name: "Chti",
        menupicture: "../assets/hamburgers/chti.jpg",
        bigpicture: "../assets/hamburgers/moyenchti.jpeg",
        littlepicture: "../assets/hamburgers/minichti.jpeg",
        epices: "non épicé",
        ingredients: ["Steak", "Sauce barbecue", "Cheddar"],
        pricealacard: 8.90,
        priceinmenu: 12.90
    },
    {
        id: 5,
        name: "Suprême",
        menupicture: "../assets/hamburgers/supreme.jpg",
        bigpicture: "../assets/hamburgers/moyensupreme.jpeg",
        littlepicture: "../assets/hamburgers/minisupreme.jpeg",
        epices: "non épicé",
        ingredients: ["Steak", "Sauce barbecue", "Cheddar"],
        pricealacard: 8.90,
        priceinmenu: 12.90
    },
    {
        id: 6,
        name: "Indien",
        menupicture: "../assets/hamburgers/indien.jpg",
        bigpicture: "../assets/hamburgers/moyenindien.jpeg",
        littlepicture: "../assets/hamburgers/miniindien.jpeg",
        epices: "non épicé",
        ingredients: ["Steak", "Sauce barbecue", "Cheddar"],
        pricealacard: 8.90,
        priceinmenu: 12.90
    },
    {
        id: 7,
        name: "Spicy",
        menupicture: "../assets/hamburgers/spicy.jpg",
        bigpicture: "../assets/hamburgers/moyenspicy.jpeg",
        littlepicture: "../assets/hamburgers/minispicy.jpeg",
        epices: "non épicé",
        ingredients: ["Steak", "Sauce barbecue", "Cheddar"],
        pricealacard: 8.90,
        priceinmenu: 12.90
    },
    {
        id: 8,
        name: "Végé",
        menupicture: "../assets/hamburgers/vege.jpg",
        bigpicture: "../assets/hamburgers/moyenvege.jpeg",
        littlepicture: "../assets/hamburgers/minivege.jpeg",
        epices: "non épicé",
        ingredients: ["Steak", "Sauce barbecue", "Cheddar"],
        pricealacard: 8.90,
        priceinmenu: 12.90
    }
];

export default PRODUCTS;
