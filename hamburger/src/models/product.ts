export default class Product {
    // 1. Typage des propiétés d'un produit.
    id: number;
    name: string;
    menupicture: string;
    bigpicture: string;
    littlepicture: string;
    epices: string;
    ingredients: Array<string>;
    pricealacard: number;
    priceinmenu: number;

    // 2. Définition des valeurs par défaut des propriétés d'un Hamburger.
    constructor(
        id: number,
        name: string = '...',
        menupicture: string = '..',
        bigpicture: string = '..',
        littlepicture: string = '..',
        epices: string = '',
        ingredients: Array<string> = ['Normal'],
        pricealacard: number = 0,
        priceinmenu: number = 0
    ) {
        // 3. Initialisation des propiétés d'un Hamburger.
        this.id = id;
        this.name = name;
        this.menupicture = menupicture;
        this.bigpicture = bigpicture;
        this.littlepicture = littlepicture;
        this.epices = epices;
        this.ingredients = ingredients;
        this.pricealacard = pricealacard;
        this.priceinmenu = priceinmenu;
    }
}
