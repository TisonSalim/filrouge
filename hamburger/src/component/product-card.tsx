import React, { FunctionComponent, useState } from "react";
import "./style.css";
import Product from "../models/product";

type Props = {
  product: Product;
  handleChange: (id: number, name: string, menupicture: string, bigpicture: string, littlepicture: string, epices: string, ingredients: Array<string>, pricealacard: number, priceinmenu: number) => void;
};

const ProductCard: FunctionComponent<Props> = ({
  product,
  handleChange,
}: Props) => {
  const catchDetails = () => {
    handleChange(product.id, product.name, product.menupicture, product.bigpicture, product.littlepicture, product.epices, product.ingredients, product.pricealacard, product.priceinmenu);
    console.log(product.name);
  };

  return (
    <div className="card  m-3">
      <div className="view overlay">
        <img
          className="card-img-top"
          src={product.menupicture}
          alt={product.name}
        />
        <div className="mask rgba-white-slight"></div>
      </div>

      <div className="card-body">
        <h4 className="card-title ">{product.name}</h4>
        {product.ingredients.map((ingredient) => (
          <span className="card-text" key={ingredient}>
            {ingredient}{" "}
          </span>
        ))}
        <a
          className="black-text d-flex justify-content-end"
          onClick={catchDetails}
        >
          <h5>
            Voir plus
            <i className="fas fa-angle-double-right" />
          </h5>
        </a>
      </div>
    </div>
  );
};

export default ProductCard;
