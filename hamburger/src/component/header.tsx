import {FunctionComponent} from "react";
import React from "react";
import {NavLink} from "react-router-dom";


const Header : FunctionComponent = ()=> {


    return (
        <header className="navbar navbar-expand-lg navbar-light  light-blue darken-3">
            <a className="navbar-brand" href="/">FabrikBurger</a>
            <div className="navbar navbar-expand">
                <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <NavLink to="/produits" className="nav-link"> Hamburgers </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/carte" className="nav-link"> Accompagnement</NavLink>
                    </li>
                </ul>
            </div>
        </header>
    );
}


export default Header;
