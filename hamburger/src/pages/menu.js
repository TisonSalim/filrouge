import React, { Component } from "react";
import "./style.css";
import { Link, Element } from "react-scroll";
import StickyBox from "react-sticky-box";
import {products} from "../models/mock-products";

const categories = products;

export default class HorizontalScroll extends Component {
  constructor(props) {
    super(props);
    categories.forEach((category) => {
      this[category.id] = React.createRef();
    });
  }

  scrollToCategory = (id) => {
    this[id].current.scrollIntoView({ inline: "center" });
  };

  render() {
    return (
      <>
        {" "}
        <StickyBox className="zindex">
          <ul
            style={{
             /* marginTop: "10px",*/
              display: "flex",
              flexDirection: "row",
              overflowY: "hidden",
              whiteSpace: "nowrap",
              listStyleType: "none",
              paddingLeft: "20px",
              backgroundColor: "#e2e2e2",
              flexWrap: "nowrap",
              height: "70px",
              justifyItems: "center",
            }}
          >
            {categories.map((category) => (
              <li
                key={category.id}
                style={{
                  display: "inline-block",
                  margin: "20px",
                }}
                ref={this[category.id]}
              >
                <Link
                  activeClass="activeCategoryLink"
                  className={category.id}
                  to={category.id.toString()}
                  spy={true}
                  smooth={true}
                  duration={500}
                  offset={-50}
                  onSetActive={() => this.scrollToCategory(category.id)}
                >
                  {category.name}
                </Link>
              </li>
            ))}
          </ul>
        </StickyBox>
        <div style={{ marginTop: "30px" }}>
          {categories.map((category) => (
            <Element
              name={category.id.toString()}
              className={category.id}
              key={"display" + category.id}
            >
              <div>                 {/* <!-- style={{ height: "50vh" }}  */}
                <h2 className="d-flex justify-content-center">
                  {category.name}
                </h2>
                {category.products.map((hamburger) => (
                  <div className="container my-2">
                  <div className="row justify-content-between">
                  <span className="col-4">
                    {hamburger.name}
                    <p>{hamburger.pricealacard}{hamburger.priceinmenu}</p>
                  </span>
                  <img className="col-4 img-rounded-corners"  src={hamburger.littlepicture} >
                  </img>

                  </div>
                  </div>
                ))}
              </div>
            </Element>
          ))}
        </div>
      </>
    );
  }
}
