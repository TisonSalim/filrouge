import React, { FunctionComponent, useState, useEffect } from 'react';
import { RouteComponentProps, Link } from 'react-router-dom';
import Loader from "../component/loader";
import Product from "../models/product";
import "../App.css"
import ProductService from "../services/product-service";



type Params = { id: string };

const ProductDetails: FunctionComponent<RouteComponentProps<Params>> = ({ match }) => {

    const [product, setProduct] = useState<Product|null>(null);



    return (
        <div >
            { product ? (
                <div className="container">
                <div className="card mb-3 mt-3">
                                <img className="card-img-top" src={product.bigpicture} alt={product.name}/>
                            <div className="card-stacked">
                                <div className="card-content">
                                    <table className="bordered striped">
                                        <tbody>
                                        <tr>
                                            <td>Nom</td>
                                            <td><strong>{ product.name }</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Epices</td>
                                            <td><strong>{ product.epices }</strong></td>
                                        </tr>
                                            <td>Ingrédients</td>
                                            <td>
                                                {product.ingredients.map(ingredient => (
                                                    <p key={ingredient} >{ingredient}</p>
                                                ))}</td>
                                        </tbody>
                                    </table>
                                </div>
                                <div className="card-action">
                                    <Link to="/produits" >Retour</Link>
                                </div>
                            </div>
                    </div>
                </div>

            ) : (
                <h4 className="center"><Loader/></h4>
            )}
        </div>

    );
}

export default ProductDetails;
