import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimerCoursComponent } from './supprimer-cours.component';

describe('SupprimerCoursComponent', () => {
  let component: SupprimerCoursComponent;
  let fixture: ComponentFixture<SupprimerCoursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupprimerCoursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimerCoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
