import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimerRoleComponent } from './supprimer-role.component';

describe('SupprimerRoleComponent', () => {
  let component: SupprimerRoleComponent;
  let fixture: ComponentFixture<SupprimerRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupprimerRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimerRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
