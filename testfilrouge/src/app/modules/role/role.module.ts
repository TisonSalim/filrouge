import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleListComponent } from './role-list/role-list.component';
import { ModifierNiveauComponent } from './modifier-role/modifier-niveau.component';
import { SupprimerRoleComponent } from './supprimer-role/supprimer-role.component';
import { AjouterRoleComponent } from './ajouter-role/ajouter-role.component';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [RoleListComponent, ModifierNiveauComponent, SupprimerRoleComponent, AjouterRoleComponent],
  imports: [
    CommonModule,
    FormsModule
  ], exports : [

  ]
})
export class RoleModule { }
