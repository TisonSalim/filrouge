import { Component, OnInit } from '@angular/core';
import {Role} from '../../../models/role';
import {RoleService} from '../../../services/role.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-ajouter-role',
  templateUrl: './ajouter-role.component.html',
  styleUrls: ['./ajouter-role.component.css']
})
export class AjouterRoleComponent implements OnInit {
role: Role;
  constructor(private roleService: RoleService, private router: Router) { }

  ngOnInit(): void {
    this.role = new Role();
  }

  ajout(){
    this.roleService.add(this.role).subscribe(
      res => {
        this.router.navigateByUrl('/role');
      });
  }
}
