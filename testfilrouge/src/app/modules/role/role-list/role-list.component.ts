import { Component, OnInit } from '@angular/core';
import {Role} from '../../../models/role';
import {RoleService} from '../../../services/role.service';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css']
})
export class RoleListComponent implements OnInit {
  roles: Role[] = [];
  constructor(private roleService: RoleService) {}

  ngOnInit(): void {
    this.roleService.getAll().subscribe(
      res => {
        this.roles = res;
      }
    );
  }

  // tslint:disable-next-line:variable-name
  supprimer(id: number): void {
    if (confirm('voulez vous vraiment supprimer ?')){
      this.roleService.delete(id).subscribe(
        res => {
          this.roleService.getAll().subscribe(
            tes => {
              this.roles = tes;
            }
          );
        },
          err => {
            alert('il y a une erreur : ' + err);
          }
      );
    }
   }
}
