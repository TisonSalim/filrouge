import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NiveauListComponent} from './niveau-list/niveau-list.component';
import { SupprimerNiveauComponent } from './supprimer-niveau/supprimer-niveau.component';
import { ModifierNiveauComponent } from './modifier-niveau/modifier-niveau.component';



@NgModule({
  declarations: [
    NiveauListComponent,
    SupprimerNiveauComponent,
    ModifierNiveauComponent
  ],
  imports: [
    CommonModule
  ]
})
export class NiveauModule { }
