import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimerNiveauComponent } from './supprimer-niveau.component';

describe('SupprimerNiveauComponent', () => {
  let component: SupprimerNiveauComponent;
  let fixture: ComponentFixture<SupprimerNiveauComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupprimerNiveauComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimerNiveauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
