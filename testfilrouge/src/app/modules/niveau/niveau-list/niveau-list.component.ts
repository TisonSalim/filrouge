import { Component, OnInit } from '@angular/core';
import {Niveau} from '../../../models/niveau';
import {NiveauService} from '../../../services/niveau.service';

@Component({
  selector: 'app-niveau-list',
  templateUrl: './niveau-list.component.html',
  styleUrls: ['./niveau-list.component.css']
})
export class NiveauListComponent implements OnInit {
  niveaux: Niveau[] = [];

  constructor(private niveauService: NiveauService) { }

  ngOnInit(): void {
    this.niveauService.getAll().subscribe(
      res => {
        this.niveaux = res;
      }
    );
  }

}
