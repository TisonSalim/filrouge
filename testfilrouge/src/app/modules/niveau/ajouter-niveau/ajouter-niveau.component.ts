
import { Component, OnInit } from '@angular/core';
import {Niveau} from '../../../models/niveau';

import {Router} from '@angular/router';
import {Role} from '../../../models/role';
import {NiveauService} from '../../../services/niveau.service';

@Component({
  selector: 'app-ajouter-niveau',
  templateUrl: './ajouter-niveau.component.html',
  styleUrls: ['./ajouter-niveau.component.css']
})
export class AjouterNiveauComponent implements OnInit {
  niveau: Niveau;
  constructor(private niveauService: NiveauService, private router: Router) { }

  ngOnInit(): void {
    this.niveau = new Niveau ();
  }

  ajout(){
    // @ts-ignore
    this.niveauService.add(this.niveau).subscribe(
      res => {
        this.router.navigateByUrl('/niveau');
      });
  }

}
