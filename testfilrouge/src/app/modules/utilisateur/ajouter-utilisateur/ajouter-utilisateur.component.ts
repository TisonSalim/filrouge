import { Component, OnInit } from '@angular/core';
import {Utilisateur} from '../../../models/utilisateur';
import {UtilisateurService} from '../../../services/utilisateur.service';
import {Router} from '@angular/router';
import {Niveau} from '../../../models/niveau';
import {Role} from '../../../models/role';
import {NiveauService} from '../../../services/niveau.service';
import {RoleService} from '../../../services/role.service';

@Component({
  selector: 'app-ajouter-utilisateur',
  templateUrl: './ajouter-utilisateur.component.html',
  styleUrls: ['./ajouter-utilisateur.component.css']
})
export class AjouterUtilisateurComponent implements OnInit {
  roles: Role[];
  niveaux: Niveau[];
  utilisateur: Utilisateur;

  constructor(private utilisateurService: UtilisateurService,
              private router: Router,
              private roleService: RoleService,
              private niveauService: NiveauService ) { }

  ngOnInit(): void {
    this.utilisateur = new Utilisateur();
    this.utilisateur.niveau = new Niveau();
    this.utilisateur.role = new Role();
    this.roleService.getAll().subscribe(
      res => {
        this.roles = res;
        console.log(this.roles);
      });
    this.niveauService.getAll().subscribe(
      res => {
        this.niveaux = res;
        console.log(this.niveaux);
      });
  }

  ajout() {
  this.utilisateurService.add(this.utilisateur).subscribe(
    res => {
      this.router.navigateByUrl('/utilisateur');
    }
  );
  }
}
