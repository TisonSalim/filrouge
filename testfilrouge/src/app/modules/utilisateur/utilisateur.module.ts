import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AjouterUtilisateurComponent } from './ajouter-utilisateur/ajouter-utilisateur.component';
import { SupprimerUtilisateurComponent } from './supprimer-utilisateur/supprimer-utilisateur.component';
import { ModifierUtilisateurComponent } from './modifier-utilisateur/modifier-utilisateur.component';
import { UtilisateurListComponent } from './utilisateur-list/utilisateur-list.component';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [AjouterUtilisateurComponent, SupprimerUtilisateurComponent, ModifierUtilisateurComponent, UtilisateurListComponent],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class UtilisateurModule { }
