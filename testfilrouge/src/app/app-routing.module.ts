import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {RoleListComponent} from './modules/role/role-list/role-list.component';
import {NiveauListComponent} from './modules/niveau/niveau-list/niveau-list.component';
import {AjouterNiveauComponent} from './modules/niveau/ajouter-niveau/ajouter-niveau.component';
import {AjouterRoleComponent} from './modules/role/ajouter-role/ajouter-role.component';
import {UtilisateurListComponent} from './modules/utilisateur/utilisateur-list/utilisateur-list.component';
import {AjouterUtilisateurComponent} from './modules/utilisateur/ajouter-utilisateur/ajouter-utilisateur.component';
import {ListCoursComponent} from './modules/cours/list-cours/list-cours.component';
import {AjouterCoursComponent} from './modules/cours/ajouter-cours/ajouter-cours.component';
import {AfficherCoursComponent} from './modules/cours/afficher-cours/afficher-cours.component';


const routes: Routes = [
  { path: 'role', component: RoleListComponent },
  { path: 'niveau', component: NiveauListComponent },
  { path: 'ajouter-niveau', component: AjouterNiveauComponent },
  { path: 'ajouter-role', component: AjouterRoleComponent },
  {path: 'utilisateur', component: UtilisateurListComponent},
  {path: 'ajouter-utilisateur', component: AjouterUtilisateurComponent},
  {path: 'cours', component: AfficherCoursComponent},
  {path: 'ajouter-cours', component: AjouterCoursComponent}
];
@NgModule({
  imports: [
   RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
