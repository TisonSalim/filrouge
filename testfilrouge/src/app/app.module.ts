// @ts-ignore
import { BrowserModule } from '@angular/platform-browser';
// @ts-ignore
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RoleModule} from './modules/role/role.module';
import { AppRoutingModule } from './app-routing.module';
import {NiveauModule} from './modules/niveau/niveau.module';
import {UtilisateurModule} from './modules/utilisateur/utilisateur.module';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { NiveauListComponent } from './modules/niveau/niveau-list/niveau-list.component';
import {NavigationModule} from './modules/navigation/navigation.module';
import { AjouterNiveauComponent } from './modules/niveau/ajouter-niveau/ajouter-niveau.component';
import {FormsModule} from '@angular/forms';
import { ListCoursComponent } from './modules/cours/list-cours/list-cours.component';
import { AfficherCoursComponent } from './modules/cours/afficher-cours/afficher-cours.component';
import { SupprimerCoursComponent } from './modules/cours/supprimer-cours/supprimer-cours.component';
import { ModifierCoursComponent } from './modules/cours/modifier-cours/modifier-cours.component';
import { AjouterCoursComponent } from './modules/cours/ajouter-cours/ajouter-cours.component';

// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    AjouterNiveauComponent,
    ListCoursComponent,
    AfficherCoursComponent,
    SupprimerCoursComponent,
    ModifierCoursComponent,
    AjouterCoursComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        RoleModule,
        NiveauModule,
        UtilisateurModule,
        RouterModule,
        HttpClientModule,
        NavigationModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
