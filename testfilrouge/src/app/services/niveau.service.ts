import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Niveau} from '../models/niveau';
import {Utilisateur} from '../models/utilisateur';

@Injectable({
  providedIn: 'root'
})
export class NiveauService {
  url = 'http://localhost:8080/';
  constructor(private http: HttpClient) { }
  getAll(): Observable<any> {
    return this.http.get(this.url + 'niveaux');
  }

  add(niveau: Niveau) {
    return this.http.post(this.url + 'niveau', niveau);
  }
}
