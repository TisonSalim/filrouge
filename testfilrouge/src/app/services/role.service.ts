import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Role} from '../models/role';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  url = 'http://localhost:8080/';
  constructor(private http: HttpClient) { }
  getAll(): Observable<any> {
    return this.http.get(this.url + 'roles');
  }

  add(role: Role) {
   return this.http.post(this.url + 'role', role);
  }

  delete(id: number): Observable<any>  {
      return this.http.delete(this.url + 'role/' + id);
  }
}
