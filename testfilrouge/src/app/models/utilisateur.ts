import {Role} from './role';
import {Niveau} from './niveau';

export class Utilisateur {

  id: number;
  nom: string;
  prenom: string;
  mail: string;
  mdp: string;
  role: Role;
  niveau: Niveau;


  constructor(id?: number, nom?: string, prenom?: string, mail?: string, mdp?: string, role?: Role, niveau?: Niveau) {
    this.id = id;
    this.nom = nom;
    this.prenom = prenom;
    this.mail = mail;
    this.mdp = mdp;
    this.role = role;
    this.niveau = niveau;
  }
}
