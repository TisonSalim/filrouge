export class Role {
  id: number;
  description: string;
  label: string;

  constructor(id?: number, description?: string, label?: string) {
    this.id = id;
    this.description = description;
    this.label = label;
  }
}
